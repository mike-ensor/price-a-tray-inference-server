#!/bin/env python

from waitress import serve
from app import create_app
import os

from config import Config

if __name__ == "__main__":
    print("Starting server...")
    application = create_app()
    print("Application Created...")
    print("Serving Model: " + Config.EDGE_MODEL_PATH)
    print("======================================================================")
    serve(
        application,
        host="0.0.0.0",
        port=int(Config().APP_PORT),
        threads=8,
        url_prefix="/",
    )
