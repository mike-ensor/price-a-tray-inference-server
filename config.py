import os


class Config:

    IMAGE_DOWNLOAD_FOLDER = os.environ.get("LABEL_STUDIO_IMAGE_DOWNLOAD_FOLDER", "/tmp/price-a-tray-images")
    APP_PORT = int(os.environ.get("APP_PORT", "5001"))

    LOG_COLOR = True
    HTTP_LOG_LEVEL = os.environ.get("HTTP_LOG_LEVEL", "INFO")
    APP_LOG_LEVEL = os.environ.get("APP_LOG_LEVEL", "INFO")

    # Google project information
    LABEL_STUDIO_HOST = os.environ.get("LABEL_STUDIO_HOST", "0.0.0.0")
    LABEL_STUDIO_PORT = os.environ.get("LABEL_STUDIO_PORT", "8080")

    GOOGLE_PROJECT = os.environ.get('PROJECT_ID')
    GOOGLE_REGION = os.environ.get('REGION', 'us-central1')
    GOOGLE_BUCKET_NAME = os.environ.get('AI_TRAINING_BUCKET_NAME', "price-a-tray-training-images")

    # Model Details (means setting up the server to run one type...may not be advisable, but will work now)
    MODEL_TYPE = os.environ.get('MODEL_TYPE', 'edge')  # vs "cloud" vs "tpu" vs "GPU"
    MODEL_NAME = os.environ.get('MODEL_NAME', "price-a-tray-{}".format(MODEL_TYPE))
    MODEL_PUBLIC_NAME = os.environ.get('MODEL_PUBLIC_NAME', 'not-set')
    MODEL_VERSION = os.environ.get('MODEL_VERSION', '0.0.1')
    MODEL_TENSOR_WIDTH = int(os.environ.get('MODEL_TENSOR_WIDTH', "512"))
    MODEL_TENSOR_HEIGHT = int(os.environ.get('MODEL_TENSOR_HEIGHT', "512"))
    MODEL_CONFIDENCE_MINIMUM = float(os.environ.get('MODEL_CONFIDENCE_MINIMUM', 0.5))

    # Edge Model
    EDGE_MODEL_PATH = os.environ.get('MODEL_PATH', 'models/model.tflite')
    EDGE_LABELS_PATH = os.environ.get('LABELS_PATH', 'models/labels.txt')

    # Cloud Model Details
    CLOUD_MODEL_URL = os.environ.get('MODEL_ENDPOINT', "{}-aiplatform.googleapis.com".format(GOOGLE_REGION))
    CLOUD_ENDPOINT_ID = os.environ.get('AI_ENDPOINT_ID', "4672924418048000")

    FEEDBACK_PATH = os.environ.get('FEEDBACK_PATH', '/feedback')
    FEEDBACK_PCT = int(os.environ.get('FEEDBACK_PCT', "1"))
