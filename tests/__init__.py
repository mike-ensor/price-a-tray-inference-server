# package for tests
import os


class TestingUtils:

    @staticmethod
    def get_testing_prefix() -> str:

        file_dir = os.path.dirname(os.path.abspath('__file__'))

        # If the test is run from the "tests" folder
        if 'tests' in file_dir:
            return "../"
        else:
            return ""
