import unittest

from app.main.controller import MLController


class TestMLController(unittest.TestCase):
    def test_default(self):
        obj = MLController()
        self.assertNotEqual(obj, None)


if __name__ == '__main__':
    unittest.main()
