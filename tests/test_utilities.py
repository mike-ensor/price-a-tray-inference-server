import unittest

from app.main.utilities import Utilities


class TestEdgeInferenceModel(unittest.TestCase):

    def test_base64_decode(self):
        expected = b'your text'

        encoded_string = 'eW91ciB0ZXh0'
        result = Utilities.base64_decode(encoded_string)

        self.assertEqual(expected, result)

    def test_base64_encode(self):
        expected = 'eW91ciB0ZXh0'

        byte_string = b'your text'

        result = Utilities.base64_encode_web_safe(byte_string)

        self.assertEqual(expected, result)


if __name__ == '__main__':
    unittest.main()
