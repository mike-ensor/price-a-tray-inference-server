import unittest

from app.main.controller_models import ModelParameters
from app.main.inference_models import EdgeInferenceModel, BaseModelClass
from app.main.utilities import Utilities
from config import Config
import cv2
import numpy as np
import base64

from tests import TestingUtils


class TestEdgeInferenceModel(unittest.TestCase):

    def test_default_class(self):
        print("FILE: " + __file__)
        os_model_path = Utilities.get_full_os_path("{}models/model.tflite".format(TestingUtils.get_testing_prefix()))
        model_parameters = ModelParameters(0.5,['mc_cheese', 'mc_coffee'])
        obj = EdgeInferenceModel(model_parameters, os_model_path)
        self.assertNotEqual(obj, None)
        self.assertTrue(issubclass(type(obj), BaseModelClass))

    def test_resize(self):
        print("FILE: " + __file__)
        os_model_path = Utilities.get_full_os_path("{}models/model.tflite".format(TestingUtils.get_testing_prefix()))
        model_parameters = ModelParameters(0.5,['mc_cheese', 'mc_coffee'])
        obj = EdgeInferenceModel(model_parameters, os_model_path)
        os_path_image = Utilities.get_full_os_path(
            "{}tests/images/single/spicy_buffalo.png".format(TestingUtils.get_testing_prefix()))
        
        # image = cv2.imread(os_path_image, cv2.IMREAD_COLOR)

        # # Encode the processed image to base64
        # encoded_image = base64.b64encode(image.tobytes()).decode()
        # new_image, width_ratio, height_ratio= obj.downsize(encoded_base64_image=encoded_image, width=512, height=512)
        # print("Original Width: {} Original Height: {}".format(width_ratio, height_ratio))
        self.assertNotEqual(obj, None)
        self.assertTrue(issubclass(type(obj), BaseModelClass))

    def test_inference_test_image(self):
        os_label_file_path = Utilities.get_full_os_path(
            "{}app/labels.txt".format(TestingUtils.get_testing_prefix()))
        with open(os_label_file_path, "r") as f:
            labels = f.read().splitlines()

        os_model_path = Utilities.get_full_os_path(
            "{}models/model.tflite".format(TestingUtils.get_testing_prefix()))
        model_parameters = ModelParameters(0.5, labels)

        model = EdgeInferenceModel(model_parameters, os_model_path)
        model_metadata = model.get_metadata()

        os_file_image = Utilities.get_full_os_path(
            "{}tests/images/single/spicy_buffalo.png".format(TestingUtils.get_testing_prefix()))

        # Turn image into base64 string
        image_data = Utilities.base64_encode_image(os_file_image, model_metadata.get_width(), model_metadata.get_height())

        response = model.predict(image_data, None)
        found_labels = response.get_labels()

        self.assertEquals(len(found_labels), 1)

    def test_inference_test_image_four_items(self):
        os_label_file_path = Utilities.get_full_os_path(
            "{}app/labels.txt".format(TestingUtils.get_testing_prefix()))
        with open(os_label_file_path, "r") as f:
            labels = f.read().splitlines()

        os_model_path = Utilities.get_full_os_path(
            "{}models/model.tflite".format(TestingUtils.get_testing_prefix()))
        model_parameters = ModelParameters(0.5, labels)


        model = EdgeInferenceModel(model_parameters, os_model_path)
        model_metadata = model.get_metadata()

        os_file_image = Utilities.get_full_os_path(
            "{}tests/images/combined/hash_honey_spicy_cheeseburger.png".format(TestingUtils.get_testing_prefix()))

        # Turn image into base64 string
        image_data = Utilities.base64_encode_image(os_file_image, model_metadata.get_width(), model_metadata.get_height())

        response = model.predict(image_data, None)
        found_labels = response.get_labels()

        self.assertEquals(len(found_labels), 4)


if __name__ == '__main__':
    unittest.main()
