#!/usr/bin/bash

# Create the feedback folder

mkdir -p ${FEEDBACK_PATH}

# Download the configured model
pushd model-puller
  # make sure all env variables are set
  source .envrc
  # ensure source folder is there
  mkdir -p "${DEST_FOLDER}"
  # Show the model values
  echo "Pulling from: gs://${MODEL_BUCKET}/${MODEL_FOLDER}/${MODEL_VERSION}"
  # Pull the model
  ./pull-model.sh
popd