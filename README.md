# Overview

This is a simple inference server that takes in one image, performs inferencing on that image and returns
bounding boxes and corresponding labels.

Secondary use is as a ML Backend for Label Studio since the overlap with inferencing is high. The Blueprint (API) for
Label Studio is in a separate space.

## API Endpoints

```shell
# see the routes for the app
flask routes
```


### Primary

`/health` -- Determine the health of the service
`/inference` -- takes in a Base64 encoded image to be inferenced and returns a JSON object of `InferenceResponse` type
`/base64` -- Turns a binary array image into a Base64 image that has been sized to 320x320 (DEPRECATED - This is a
helper)

### Label Studio add-on

Approach for Label Studio is to perform the inferencing offline from the request. Once inferenced, the system will
call back Laabel Studio via API (credentials obtained in `/setup`) with the relevant information for each `Task`.

`/setup` -- Used to setups the service and is called by LabelStudio to provide important links back to Label Studio
`/predict` -- Similar to `/inference` above but is called with an array of `tasks` from Label Studio

## Getting started

This project uses Python 3.10 and is written as a Flask app. The models used are TFLite based models.

## Unit Tests

* Running all unit tests

    ```shell
    python -m unittest discover -s tests
    ```


## Series Links

This project is one component in a multi-component solution. The composed parts make up the game "Price a Tray". Note, this inference server is also used in "Build-a-Thing" game https://gitlab.com/mike-ensor/build-a-thing-package.

https://gitlab.com/mike-ensor/price-a-tray-dynamic-frontend
: This is the User Experience and User Interface for the game

https://gitlab.com/mike-ensor/price-a-tray-backend
: This is the Game API where rules, game lifecycle, scores and data are stored

https://gitlab.com/mike-ensor/rtsp-to-mjpeg.git
: This project turns a RTSP stream into a MJPEG stream for consumption by the UI

https://gitlab.com/mike-ensor/price-a-tray-game-package
: This is the project used to deploy to a Kubernetes project with ConfigSync installed

https://gitlab.com/mike-ensor/price-a-tray-inference-server
: This project provides an API to inference extraced frames (image) against the edge model pulled from the GCS bucket.

