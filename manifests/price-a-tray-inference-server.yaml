apiVersion: apps/v1
kind: Deployment
metadata:
  annotations:
    author: Mike Ensor
    origin: https://gitlab.com/mike-ensor/price-a-tray-inference-server
  name: inference-service-deployment
  namespace: price-a-tray
spec:
  replicas: 1
  selector:
    matchLabels:
      app: inference-server-service
  template:
    metadata:
      annotations:
        author: Mike Ensor
        origin: https://gitlab.com/mike-ensor/price-a-tray-inference-server
        prometheus.io/path: /
        prometheus.io/port: "9090"
        prometheus.io/scrape: "false"
      labels:
        app: inference-server-service
        type: inference-service
    spec:
      containers:
      - envFrom:
        - configMapRef:
            name: inf-server-config-b5tgd4dd9m
        image: registry.gitlab.com/mike-ensor/price-a-tray-inference-server/inference-server:v1.0.7
        imagePullPolicy: IfNotPresent
        livenessProbe:
          httpGet:
            path: /health
            port: 5001
          initialDelaySeconds: 1
          periodSeconds: 5
        name: server
        readinessProbe:
          httpGet:
            path: /health
            port: 5001
          initialDelaySeconds: 2
          periodSeconds: 5
        resources:
          limits:
            cpu: 4
            memory: 4Gi
          requests:
            cpu: 500m
            memory: 500Mi
        securityContext:
          allowPrivilegeEscalation: false
          readOnlyRootFilesystem: true
        volumeMounts:
        - mountPath: /tmp
          name: image-download-temp-volume
        - mountPath: /downloads
          name: models-volume
        - mountPath: /feedback
          name: feedback-volume
      initContainers:
      - envFrom:
        - configMapRef:
            name: model-puller-config-2k2tgk7kd6
        image: registry.gitlab.com/mike-ensor/price-a-tray-inference-server/model-puller:v1.0.0
        name: model-puller
        volumeMounts:
        - mountPath: /google/credentials.json
          name: google-credentials
          readOnly: true
          subPath: credentials.json
        - mountPath: /downloads
          name: models-volume
      volumes:
      - emptyDir:
          sizeLimit: 750Mi
        name: image-download-temp-volume
      - name: models-volume
        persistentVolumeClaim:
          claimName: models-volume-pvc
      - name: feedback-volume
        persistentVolumeClaim:
          claimName: feedback-volume-pvc
      - name: google-credentials
        secret:
          secretName: google-model-auth
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  annotations:
    author: Mike Ensor
    origin: https://gitlab.com/mike-ensor/price-a-tray-inference-server
  name: models-volume-pvc
  namespace: price-a-tray
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  storageClassName: robin
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  annotations:
    author: Mike Ensor
    origin: https://gitlab.com/mike-ensor/price-a-tray-inference-server
  name: feedback-volume-pvc
  namespace: price-a-tray
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  storageClassName: robin
---
apiVersion: v1
kind: Service
metadata:
  annotations:
    author: Mike Ensor
    origin: https://gitlab.com/mike-ensor/price-a-tray-inference-server
  name: inference-server-service
  namespace: price-a-tray
spec:
  ports:
  - name: application
    port: 8080
    protocol: TCP
    targetPort: 5001
  selector:
    app: inference-server-service
  type: LoadBalancer
---
apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  annotations:
    author: Mike Ensor
    origin: https://gitlab.com/mike-ensor/price-a-tray-inference-server
  name: google-model-auth-external-secret
  namespace: price-a-tray
spec:
  data:
  - remoteRef:
      key: google-model-gsa-key
    secretKey: credentials.json
  refreshInterval: 24h
  secretStoreRef:
    kind: ClusterSecretStore
    name: gcp-secret-store
  target:
    creationPolicy: Owner
    name: google-model-auth
---
apiVersion: v1
data:
  APP_PORT: "5001"
  GOOGLE_APPLICATION_CREDENTIALS: /google/credentials.json
  LABEL_STUDIO_HOST: 333.11.11.11
  LABEL_STUDIO_IMAGE_DOWNLOAD_FOLDER: /tmp
  LABEL_STUDIO_PORT: "80"
  LABELS_PATH: /downloads/gcafe/0.0.2/labels.txt
  MODEL_CONFIDENCE_MINIMUM: "0.4"
  MODEL_PATH: /downloads/gcafe/0.0.2/model.tflite
  MODEL_PUBLIC_NAME: Google Cafe
  MODEL_TENSOR_HEIGHT: "512"
  MODEL_TENSOR_WIDTH: "512"
  MODEL_TYPE: edge
  MODEL_VERSION: 0.0.1
kind: ConfigMap
metadata:
  annotations:
    author: Mike Ensor
    origin: https://gitlab.com/mike-ensor/price-a-tray-inference-server
  labels:
    app-config: inference-server-primary
    config-owner: inference-server-config
    generated-by-kustomize: "true"
    owner: inference-server
  name: inf-server-config-b5tgd4dd9m
  namespace: price-a-tray
---
apiVersion: v1
data:
  DEST_FOLDER: /downloads
  GOOGLE_APPLICATION_CREDENTIALS: /google/credentials.json
  MODEL_BUCKET: price-a-tray-edge-models
  MODEL_FOLDER: gcafe
  MODEL_PUBLIC_NAME: Google Cafe
  MODEL_VERSION: 0.1.3
kind: ConfigMap
metadata:
  annotations:
    author: Mike Ensor
    origin: https://gitlab.com/mike-ensor/price-a-tray-inference-server
  labels:
    app-config: inference-server-model-puller
    config-owner: inference-server-config
    generated-by-kustomize: "true"
    owner: inference-server
  name: model-puller-config-2k2tgk7kd6
  namespace: price-a-tray
