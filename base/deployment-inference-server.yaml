apiVersion: apps/v1
kind: Deployment
metadata:
  name: inference-service-deployment
spec:
  replicas: 1 # switch to 3 later
  #  affinity:
  #    podAffinity:
  #      preferredDuringSchedulingIgnoredDuringExecution:
  #        - labelSelector:
  #            matchExpressions:
  #              - key: scheduling
  #                operator: In
  #                values:
  #                  - "zone-1"
  #          topologyKey: kubernetes.io/hostname
  selector:
    matchLabels:
      app: inference-server-service
  template:
    metadata:
      labels:
        app: inference-server-service
        type: inference-service # Common for all services
      annotations:
        # For non Google Managed Prometheus or PodMonitoring based scraping
        prometheus.io/scrape: "false"
        prometheus.io/path: "/"
        prometheus.io/port: "9090"
    spec:
      initContainers:
        - image: registry.gitlab.com/mike-ensor/price-a-tray-inference-server/model-puller
          name: model-puller
          envFrom:
            - configMapRef:
                name: model-puller-config
          # command: ['/bin/bash', '-c', 'sleep 100000']
          volumeMounts:
            - name: google-credentials
              mountPath: /google/credentials.json
              subPath: credentials.json
              readOnly: true
            - mountPath: /downloads # NOTE: matches the main container and DEST_FOLDER
              name: models-volume
      containers:
        - image: registry.gitlab.com/mike-ensor/price-a-tray-inference-server/inference-server # ${IMAGE_LATEST_FULL}
          envFrom:
            - configMapRef:
                name: inf-server-config
          imagePullPolicy: IfNotPresent
          livenessProbe:
            httpGet:
              path: /health
              port: 5001
            initialDelaySeconds: 1
            periodSeconds: 5
          name: server
          readinessProbe:
            httpGet:
              path: /health
              port: 5001
            initialDelaySeconds: 2
            periodSeconds: 5
          resources:
            limits:
              cpu: 4
              memory: 4Gi
            requests:
              cpu: 500m
              memory: 500Mi
          securityContext:
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: true
          volumeMounts:
            - mountPath: /tmp
              name: image-download-temp-volume
            - mountPath: /downloads                 # NOTE: Matches the DEST_FOLDER and same as init-container
              name: models-volume
            - mountPath: /feedback
              name: feedback-volume
      volumes:
        - name: image-download-temp-volume
          emptyDir:
            sizeLimit: 750Mi
        - name: models-volume
          persistentVolumeClaim:
            claimName: models-volume-pvc
        - name: feedback-volume
          persistentVolumeClaim:
            claimName: feedback-volume-pvc
        - name: google-credentials
          secret:
            secretName: google-model-auth

---

apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: models-volume-pvc
spec:
  storageClassName: robin
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi

---

apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: feedback-volume-pvc
spec:
  storageClassName: robin
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi

---

apiVersion: v1
kind: Service
metadata:
  name: inference-server-service
spec:
  type: LoadBalancer
  selector:
    app: inference-server-service
  ports:
    - name: application
      port: 8080
      protocol: TCP
      targetPort: 5001

---

apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: google-model-auth-external-secret
spec:
  refreshInterval: "24h"                        # Sync at 1 day
  secretStoreRef:
    kind: ClusterSecretStore
    name: gcp-secret-store
  target:                                       # K8s secret definition
    name: google-model-auth                     # K8s name
    creationPolicy: Owner
  data:
  - secretKey: credentials.json                 # File inside the K8s secret
    remoteRef:
      key: google-model-gsa-key                 # GCP Secret
