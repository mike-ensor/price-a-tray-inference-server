# Overview

This is intended to bve used as an `init-container`. The init-container will take a few parameters in
and pull down the model to the local container before the inference server starts up. If the container is
removed, then the model will be dropped.

# Bucket Access

The script requires access to the GCS bucket to pull down the models. In order to do this, a Google 
Service Account needs to be created in the target project, generate a JSON key and create a Google 
Secret Manager secret with the contents of the JSON key. The minimal GSA should have `roles/storage.objectViewer` 
access to the bucket only. This GSA will not write to the bucket, only have read-only access.

The `/manifests` folder of the outter project has KRM configuration that uses an `ExternalSecret`. That ExternalSecret pulls down 
the credentials into a Kubernetes `Secret` and makes that available for the `pull-model.sh` script inside the 
Docker image used in the init-container phase.

In your GCS bucket, the following structure convention needs to be used to store the models.

```
gs://[MODEL_BUCKET]/[MODEL_FOLDER]/[MODEL_VERSION]/[MODEL_NAME].tflite  
```

MODEL_BUCKET
: Google Cloud Storage bucket name without `gs://` prefix

MODEL_FOLDER
: Folder path at the root of the `MODEL_BUCKET` where the version folders are located.

MODEL_VERSION
: Folder name following semver (major.minor.patch) format containing the model and label for the versioned model

MODEL_NAME
: The name of the model without the `.tflite` suffix. NOTE: `.tflite` is required for the name of the model. Default is `model` if not specified


## Google Service Account & GCP Secret

The below script can be used to create a new Google Service Account, add permissions to view the desired 
bucket, create a JSON key and then create a GCP Secret ready for use by the `ExternalSecret`. 

```shell
export PROJECT_ID="<your-project>"
export GSA_NAME="price-a-tray-model-gsa"
export MODEL_BUCKET="price-a-tray-edge-models"
export FULL_GSA_EMAIL="${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com"

# Create a new GSA
gcloud iam service-accounts create ${GSA_NAME} \
     --display-name="Read-only access to models for Price a Tray" \
     --project ${PROJECT_ID} 


# Add the object viewer access
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
     --member=serviceAccount:${FULL_GSA_EMAIL} \
     --role='roles/storage.objectViewer' \
     --project ${PROJECT_ID} 

# Give Read access to the bucket
gcloud storage buckets add-iam-policy-binding gs://${MODEL_BUCKET} \
    --member=serviceAccount:${FULL_GSA_EMAIL} \
    --role=roles/storage.objectViewer

# Create a GSA Key
gcloud iam service-accounts keys create credentials.json \
     --iam-account=${GSA_NAME}@${PROJECT_ID}.iam.gserviceaccount.com \
     --project ${PROJECT_ID} 

# Create Google Secret
gcloud secrets create google-model-gsa-key \
    --data-file=./credentials.json \
    --replication-policy=automatic \
    --project ${PROJECT_ID}
```

One additional variable is the `GOOGLE_APPLICATION_CREDENTIALS` variable. This should point to the GSA key mounted as 
a volume to the container. The value should be relative to the container. 

## Running Locally

1. Copy the `.envrc-template` to `.envrc`
2. Set any of the configuration to match your bucket, folder and naming
3. Run the lower commands to setup, build and run the docker container locally

### Setup Local
```shell
# Setup the local folder to emulate the docker environment. This will be the mounted Volume
export DEST_FOLDER="/downloads"
export LOCAL_DOWNLOAD="$(pwd)${DEST_FOLDER}"

# Create the local downloads folder
mkdir -p ${LOCAL_DOWNLOAD}

# build locally
docker build -t model-puller-local .

```

### Running Docker

Make sure the Google Service Account credentials are located at the file pointed to by the GOOGLE_APPLICATION_CREDENTIALS. The 
`Secret` and `VolumeMount` will put the contents and file at `/google/credentials.json` inside the Pod when using K8s.

```shell
docker run \
       -e MODEL_BUCKET="${MODEL_BUCKET}" \
       -e MODEL_NAME="${MODEL_NAME}" \
       -e MODEL_FOLDER="${MODEL_FOLDER}" \
       -e MODEL_VERSION="${MODEL_VERSION}" \
       -e DEST_FOLDER="${DEST_FOLDER}" \
       -e GOOGLE_APPLICATION_CREDENTIALS="/google/credentials.json" \
       -v $(pwd)/credentials.json:/google/credentials.json \
       -v ${LOCAL_DOWNLOAD}:${DEST_FOLDER}/ \
       -it model-puller-local
```