#!/bin/bash

# GCS Folder Pattern needed
# gs://<bucket>/model-folder/<major>.<minor>.<version>/<name | model>.tflite && labels.txt

FINAL_MODEL_NAME="${MODEL_NAME:-model}"
FINAL_MODEL_FOLDER="${MODEL_FOLDER:-models}"
LOCAL_DESTINATION_FOLDER="${DEST_FOLDER:-/downloads}"
LOCAL_FINAL_FOLDER="${LOCAL_DESTINATION_FOLDER}/${MODEL_FOLDER}"

# This will pull a tflite model from GCS

if [[ -z "${GOOGLE_APPLICATION_CREDENTIALS}" ]]; then
  echo "GOOGLE_APPLICATION_CREDENTIALS environment variable needs to be set and pointing to a GSA "
  echo "    that has access to the target bucket. Aborting"
  exit 1
fi

if [[ -z "${MODEL_BUCKET}" ]]; then
  echo "MODEL_BUCKET environment variable needs to be set and pointing to valid GCS bucket. Aborting"
  exit 1
fi

# Model Version
if [[ -z "${MODEL_VERSION}" ]]; then
  echo "MODEL_VERSION environment variable needs to be set and pointing versioned \${MODEL_NAME}.tflite object. Aborting"
  # TODO: Look for X.Y.Z semver format
  exit 1
fi

# 3. Check if Path on local folder exist
if [[ ! -d "${LOCAL_DESTINATION_FOLDER}" ]]; then
  echo "DEST_FOLDER is was not found on the container. Aborting"
  exit 1
else
  mkdir -p "${LOCAL_FINAL_FOLDER}"
fi

echo "Model Configuration"
echo "=================================================="
gcloud auth activate-service-account --key-file="${GOOGLE_APPLICATION_CREDENTIALS}"
echo "Authenticated using ${GOOGLE_APPLICATION_CREDENTIALS} JSON key"
echo "=================================================="
echo "MODEL_BUCKET: ${MODEL_BUCKET} -- GCS Bucket models are housed in"
echo "MODEL_FOLDER: ${FINAL_MODEL_FOLDER} -- Root folder containing Version directories"
echo "MODEL_VERSION: ${MODEL_VERSION} -- Folder named in semver format containing the model and label files"
echo "MODEL_NAME: ${FINAL_MODEL_NAME}.tflite -- Name of the model in the version folder to pull"
echo "LOCAL_FINAL_FOLDER: ${LOCAL_FINAL_FOLDER} -- Local folder to pull the Model into"
echo "=================================================="
echo "Validation Checks"
echo "=================================================="

# Checks
# 1. Bucket Access
gcloud storage ls "gs://${MODEL_BUCKET}" # >/dev/null 2>&1
# shellcheck disable=SC2181
if [[ $? -gt 0 ]]; then
  echo "Cannot list the bucket ${MODEL_BUCKET}. Aborting"
  exit 1
fi

# 2. Bucket path contents
FULL_BUCKET_FOLDER="gs://${MODEL_BUCKET}/${FINAL_MODEL_FOLDER}/${MODEL_VERSION}"
gcloud storage ls "${FULL_BUCKET_FOLDER}/${FINAL_MODEL_NAME}.tflite" #>/dev/null 2>&1
# shellcheck disable=SC2181
if [[ $? -gt 0 ]]; then
  echo "Model not found at '${FULL_BUCKET_FOLDER}/${FINAL_MODEL_NAME}.tflite'. Aborting"
  exit 1
fi
gcloud storage ls "$FULL_BUCKET_FOLDER/labels.txt" #>/dev/null 2>&1
# shellcheck disable=SC2181
if [[ $? -gt 0 ]]; then
  echo "Model Labels not found at '${FULL_BUCKET_FOLDER}/labels.txt'. Aborting"
  exit 1
fi

echo "All Cloud-based checks are complete"
echo "=================================================="

# 4. Check if the model already exists, if so, skip pulling it down (unless a flag exists)
LOCAL_MODEL_PATH="${LOCAL_FINAL_FOLDER}/${MODEL_VERSION}"
if [[ -d "${LOCAL_MODEL_PATH}" ]]; then
  echo "Model has already been downloaded. Skipping"
else
  # 5. Download all of the the model versions into the local folder
  gcloud storage cp --recursive "${FULL_BUCKET_FOLDER}" "${LOCAL_FINAL_FOLDER}"
  # shellcheck disable=SC2181
  if [[ $? -gt 0 ]]; then
    echo "Error downloading the model and label from the bucket."
    exit 1
  fi
fi

# 6. Set permissions to read-only for all
chmod 444 "${LOCAL_FINAL_FOLDER}/${MODEL_VERSION}/labels.txt"
chmod 444 "${LOCAL_FINAL_FOLDER}/${MODEL_VERSION}/${FINAL_MODEL_NAME}.tflite"
