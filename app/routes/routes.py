import json
import logging

from flask import jsonify, request, Response, current_app

from config import Config
from . import bp
from . import ls_bp
from ..main.controller import MLController
from ..main.controller_models import ModelParameters
from ..main.labelstudio_datamodel import Setup
from ..main.utilities import Utilities

log = logging.getLogger(current_app.name)

# Global single instance of MLController
# NOTE: Model type is selected for the whole app instance, may not be preferable. Future, suggest
#       using custom headers or some other strategy model decided at call-time


@bp.before_app_request
def on_app_startup():
    """ This method is run before ALL other methods are run. This is a on-time setup function """
    labels = []
    os_label_file_path = Utilities.get_full_os_path(Config().EDGE_LABELS_PATH)
    with open(os_label_file_path, "r") as f:
        labels = f.read().splitlines()

    confidence = Config().MODEL_CONFIDENCE_MINIMUM
    model_parameters = ModelParameters(confidence, labels)

    # initialize MLController
    ml_controller = MLController()

    if ml_controller.is_initialized() is not True:
        ml_controller.initialize(model_parameters, Config.MODEL_TYPE)
        log.info(f"Model parameters: {model_parameters}, Model Confidence: {confidence}")


@bp.route("/")
@bp.route("/health")
def health():

    ml_controller = MLController()

    service_status = "DOWN"
    status_code = 503  # not ready

    if ml_controller.is_initialized():
        service_status = "UP"
        status_code = 200

    data = {"status": "{}".format(service_status), "v2": True}
    return Response(response=json.dumps(data),
                    status=status_code,
                    mimetype="application/json",
                    content_type='application/json')


@ls_bp.route("/setup", methods=['POST'])
def setup():
    ml_controller = MLController()

    if request.method == 'POST':
        project_info = Setup(request.get_json())
        setup_success = ml_controller.setup(project_info)
        if setup_success is not True:
            log.error("Setup was NOT successful")
        else:
            log.info("Setup was successful")
        response_object = {"model_version": "0.0.1"}
        return Response(response=json.dumps(response_object))


@ls_bp.route("/predict", methods=['POST'])
def predict_batch():
    """ Called by LabelStudio -- this should be a list of Tasks that needs to be inferenced """
    ml_controller = MLController()

    data = request.get_json()
    tasks = data['tasks']

    success = ml_controller.batch(tasks)

    log.info("/predict response: {}".format(success))
    return jsonify({"success": "true"})


@bp.route("/inference", methods=['POST'])
def inference_single_image():
    """ Called by API to inference a single image encoded in base64 format """
    ml_controller = MLController()
    data = request.json

    should_resize = request.args.get('resize', False)
    data = data.get("image")

    response = ml_controller.inference(data, should_resize)
    response_json = response.to_dict()

    return jsonify(response_json)


@bp.route("/metadata", methods=['GET'])
def get_metatdata():
    """ Get All Model """
    ml_controller = MLController()

    metadata = ml_controller.metadata()
    response_json = metadata.to_dict()

    return jsonify(response_json)
