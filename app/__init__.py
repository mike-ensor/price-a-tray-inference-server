import logging
import os
import sys

from colorlog import ColoredFormatter
from flask import Flask
from flask.logging import default_handler
from flask_cors import CORS

from config import Config


def create_app():
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    # load the instance config, if it exists, when not testing
    # app.config.from_object(config_class)

    # Set the logging for the http server
    werkzeug_log = logging.getLogger('werkzeug')
    werkzeug_log.setLevel(Config.HTTP_LOG_LEVEL)
    # Disable the web banner (kinda)
    cli = sys.modules['flask.cli']
    cli.show_server_banner = lambda *x: None

    show_color_logs = Config.LOG_COLOR
    print("Color Logs: {}".format(show_color_logs))
    if show_color_logs:
        formatter = ColoredFormatter(
            '%(log_color)s%(levelname)-8s%(reset)s %(message)s',
            datefmt='%H:%M:%S',
            log_colors={
                'DEBUG': 'cyan',
                'INFO': 'green',
                'WARNING': 'yellow',
                'ERROR': 'red',
                'CRITICAL': 'red,bold'
            }
        )

        # Create a stream handler and set the formatter
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(formatter)

        # Add the stream handler to the app's logger
        app.logger.addHandler(stream_handler)
        app.logger.removeHandler(default_handler)

    app.logger.setLevel(Config.APP_LOG_LEVEL)

    app.app_context().push()

    # Register blueprints here
    from app.routes import bp as main_bp
    from app.routes import ls_bp as ls_bp
    app.register_blueprint(main_bp)
    app.register_blueprint(ls_bp)

    # Check if the Config.FEEDBACK_PATH file exists and log to Error if not
    if not os.path.exists(Config.FEEDBACK_PATH):
        app.logger.error(f"\n\nFeedback folder (FEEDBACK_PATH) was not found: {Config.FEEDBACK_PATH}\n\n")

    # ensure the instance folder exists
    try:
        print("Starting")
        # os.makedirs(app.instance_path)
    except OSError:
        pass

    CORS(app)

    return app
