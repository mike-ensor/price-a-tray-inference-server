from config import Config


class Detected_Item:
    def __init__(self, min_x, min_y, max_x, max_y, label, confidence):
        self.min_x = min_x
        self.min_y = min_y
        self.max_y = max_y
        self.max_x = max_x
        self.label = label
        self.confidence = confidence

    def to_dict(self):
        return {
            "min_x": self.min_x,
            "min_y": self.min_y,
            "max_x": self.max_x,
            "max_y": self.max_y,
            "label": self.label,
            "confidence": self.confidence
        }

    def convert_back_to_original_size(self, original_width, original_height, image):
        pass


class InferenceResponse:
    def __init__(self, detected_items=None):
        self.detected_items = detected_items

    def __str__(self):
        return str(self.to_dict())

    def get_labels(self):  # Renamed for clarity
        return [item.label for item in self.detected_items]

    def to_dict(self):
        """Converts the InferenceResponse object into a dictionary representation."""
        return {
            "detected_items": [item.to_dict() for item in self.detected_items]
        }


class ModelParameters:

    def __init__(self, score_minimum, labels=None):
        if labels is None:
            labels = []

        self.score_minimum = score_minimum
        self.labels = labels


class Metadata:
    def __init__(self, uri, image_height, image_width, labels=None):
        if uri is None:
            self.uri = ""
        self.image_height = image_height
        self.image_width = image_width
        self.model_version = Config.MODEL_VERSION
        self.model_confidence = Config.MODEL_CONFIDENCE_MINIMUM
        self.model_name = Config.MODEL_NAME
        self.public_name = Config.MODEL_PUBLIC_NAME
        self.model_type = Config.MODEL_TYPE
        self.labels = labels

    def to_dict(self):
        return {
            "uri": self.uri,
            "image_height": str(self.image_height),
            "image_width": str(self.image_width),
            "model_name": self.model_name,
            "public_name": self.public_name,
            "model_type": self.model_type,
            "model_version": self.model_version,
            "model_confidence": self.model_confidence,
            "labels": self.labels
        }

    def get_width(self):  # Renamed for clarity
        return self.image_width

    def get_height(self):  # Renamed for clarity
        return self.image_height
