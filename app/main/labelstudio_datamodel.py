import logging
from collections import defaultdict

from flask import current_app
from label_studio_tools.core.label_config import has_variable, _is_input_tag, _LABEL_TAGS, _get_parent_output_tag_name, \
    _is_output_tag
from label_studio_tools.core.utils.exceptions import (
    LabelStudioXMLSyntaxErrorSentryIgnored,
)
from lxml import etree

log = logging.getLogger(current_app.name)


class Setup:
    def __init__(self, data):
        self.project = data['project']
        self.hostname = data['hostname']
        self.access_token = data['access_token']
        parsed_label_config = self.parse_config(data['schema'])
        self.from_name, self.schema = list(parsed_label_config.items())[0]
        self.to_name = self.schema['to_name'][0]
        self.model_version = "0.0.1" # TODO: Set this somewhere

    def __str__(self):
        response = "project: {}".format(self.project)
        response += "\nfrom_name: {}".format(self.from_name)
        response += "\nto_name: {}".format(self.to_name)
        response += "\nschema: {}".format(self.schema)
        response += "\naccess_token: {}".format(self.access_token)
        return response

    @staticmethod
    def parse_config(config_string):
        """Parse a given Label Studio labeling configuration and return a structured version of the configuration.
        Useful for formatting results for predicted annotations and determining the type(s) of ML models that might
        be relevant to the labeling project.

        :param config_string: Label config string
        :return: structured config of the form:
        {
            "<ControlTag>.name": {
                "type": "ControlTag",
                "to_name": ["<ObjectTag1>.name", "<ObjectTag2>.name"],
                "inputs: [
                    {"type": "ObjectTag1", "value": "<ObjectTag1>.value"},
                    {"type": "ObjectTag2", "value": "<ObjectTag2>.value"}
                ],
                "labels": ["Label1", "Label2", "Label3"] // taken from "alias" if it exists, else "value"
        }
        """
        if not config_string:
            return {}

        try:
            xml_tree = etree.fromstring(config_string)
        except etree.XMLSyntaxError as e:
            raise LabelStudioXMLSyntaxErrorSentryIgnored(str(e))
        inputs, outputs, labels = {}, {}, defaultdict(dict)
        # Add variables to config (e.g. {{idx}} for index in Repeater
        variables = []
        for tag in xml_tree.iter():
            if tag.attrib and 'indexFlag' in tag.attrib:
                variables.append(tag.attrib['indexFlag'])
            if _is_output_tag(tag):
                tag_info = {'type': tag.tag, 'to_name': tag.attrib['toName'].split(',')}
                if variables:
                    # Find variables in tag_name and regex if find it
                    for v in variables:
                        for tag_name in tag_info['to_name']:
                            if v in tag_name:
                                if 'regex' not in tag_info:
                                    tag_info['regex'] = {}
                                tag_info['regex'][v] = ".*"
                # Grab conditionals if any
                conditionals = {}
                if tag.attrib.get('perRegion') == 'true':
                    if tag.attrib.get('whenTagName'):
                        conditionals = {'type': 'tag', 'name': tag.attrib['whenTagName']}
                    elif tag.attrib.get('whenLabelValue'):
                        conditionals = {
                            'type': 'label',
                            'name': tag.attrib['whenLabelValue'],
                        }
                    elif tag.attrib.get('whenChoiceValue'):
                        conditionals = {
                            'type': 'choice',
                            'name': tag.attrib['whenChoiceValue'],
                        }
                if conditionals:
                    tag_info['conditionals'] = conditionals
                if has_variable(tag.attrib.get("value", "")) or tag.attrib.get("apiUrl"):
                    tag_info['dynamic_labels'] = True
                outputs[tag.attrib['name']] = tag_info
            elif _is_input_tag(tag):
                inputs[tag.attrib['name']] = {
                    'type': tag.tag,
                    'value': tag.attrib['value'].lstrip('$'),
                }
            if tag.tag not in _LABEL_TAGS:
                continue
            parent_name = _get_parent_output_tag_name(tag, outputs)
            if parent_name is not None:
                actual_value = tag.attrib.get('alias') or tag.attrib.get('value')
                if not actual_value:
                    log.debug(
                        'Inspecting tag {tag_name}... found no "value" or "alias" attributes.'.format(
                            tag_name=etree.tostring(tag, encoding='unicode').strip()[:50]
                        )
                    )
                else:
                    labels[parent_name][actual_value] = dict(tag.attrib)
        for output_tag, tag_info in outputs.items():
            tag_info['inputs'] = []
            for input_tag_name in tag_info['to_name']:
                if input_tag_name not in inputs:
                    log.info(
                        f'to_name={input_tag_name} is specified for output tag name={output_tag}, '
                        'but we can\'t find it among input tags'
                    )
                    continue
                tag_info['inputs'].append(inputs[input_tag_name])
            tag_info['labels'] = list(labels[output_tag])
            tag_info['labels_attrs'] = labels[output_tag]
        return outputs


class Task:
    def __init__(self, params):
        self.id = params["id"]
        self.data = params["data"]
        self.image = self.data["image"]
