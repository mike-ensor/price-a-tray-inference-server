import base64
import os

import cv2
import numpy as np
from config import Config


class Utilities:

    @staticmethod
    def get_full_os_path(relative_model_path):
        """ Returns the full path of the relative model provided"""
        """ :param relative_model_path is the path relative to the app base directory"""
        testing_path = os.getcwd()
        return os.path.join(testing_path, relative_model_path)

    @staticmethod
    def base64_decode(websafe_encoded_image) -> bytes:
        """ Decodes thes the encoded UTF-8 image characters to bytes"""
        return base64.b64decode(websafe_encoded_image.encode('utf-8'))

    @staticmethod
    def base64_encode_web_safe(image_bytes: bytes) -> str:
        """ Decodes thes the encoded UTF-8 image characters to bytes"""
        encoded_bytes = base64.b64encode(image_bytes)
        return encoded_bytes.decode('utf-8')


    @staticmethod
    def base64_encode_image(os_path_image, width, height) -> str:
        """ Encodes the image to base64 """
        """ :param os_path_image is the full path to the image"""
        image = cv2.imread(os_path_image, cv2.IMREAD_COLOR)

        # Resize the image to (320, 320)
        resized_image = cv2.resize(image, (width, height))

        # Convert the image to RGB format (OpenCV reads images in BGR format)
        rgb_image = cv2.cvtColor(resized_image, cv2.COLOR_BGR2RGB)

        # Reshape the image to (1, 320, 320, 3)
        processed_image = np.expand_dims(rgb_image, axis=0)

        # Encode the processed image to base64
        encoded_image = base64.b64encode(processed_image.tobytes()).decode()

        # Print the base64 encoded string
        return encoded_image
