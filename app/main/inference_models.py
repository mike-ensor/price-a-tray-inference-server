import base64
import json
import logging
import multiprocessing
import os
import random
import re
import threading
from abc import abstractmethod, ABC
from datetime import datetime
from typing import List, Optional

import cv2
import numpy as np
import requests
import tensorflow as tf
import yaml
from flask import current_app
from google.cloud import storage

from app.main.controller_models import InferenceResponse, ModelParameters, Detected_Item, Metadata
from app.main.labelstudio_datamodel import Task, Setup
from config import Config

log = logging.getLogger(current_app.name)

# Convenience variables to make pipe read/write easier to read
INPUT_PIPE = 0
OUTPUT_PIPE = 1

threadLock = threading.Lock()


class BaseModelClass(ABC):

    @abstractmethod
    def predict(self, image_data, resize) -> InferenceResponse:
        pass

    @abstractmethod
    def setup(self, info) -> bool:
        return False

    @abstractmethod
    def get_metadata(self) -> Metadata:
        pass

    @abstractmethod
    def batch_inference(self, tasks: List) -> bool:
        return False


class CloudInferenceModel(BaseModelClass):
    """ This is a class to perform Cloud based model inferencing/predictions based on AI Platform"""

    def __init__(self, model_params: ModelParameters):
        self.model_version = "0.0.1"  # TODO: Put this in ModelParameters or something
        self.model_params = model_params

    def setup(self, info):
        # TODO: Anything needed to setup?
        log.info("CloudInferenceModel Setup with: {}".format(info))
        return False

    def batch_inference(self, tasks: List) -> bool:
        log.info(f"TODO: Batch inference for Cloud Based Inferencing")
        return False

    def get_metadata(self) -> Metadata:
        return Metadata(Config.CLOUD_ENDPOINT_ID, 1, 1)

    def metadata(self):
        # TODO: Any Cloud Based Metadata
        pass

    def predict(self, raw_image_data, resize) -> InferenceResponse:
        # 1. Encode image data to be base64
        # 2. Call AI Model endpoint w/ parameters needed
        # TODO: thie is an unused or missing implementation function
        # _encoded_image = Utilities.base64_encode_image(raw_image_data, resize)

        return InferenceResponse()


class EdgeInferenceModel(BaseModelClass):
    """ This is a class to perform Edge based .tflite based model inferencing/predictions"""

    def __init__(self, model_params: ModelParameters, model_path):
        if os.path.exists(model_path):
            self.model_path = model_path
        else:
            # Boot out early if no path is supplied
            raise Exception("Edge Model Path is not valid")

        self.model_params = model_params
        self.model_version = None
        # Get the model shape and store for longer trem
        interpreter = tf.lite.Interpreter(model_path=model_path)
        input_details = interpreter.get_input_details()
        input_shape = input_details[0]['shape']
        self.model_tensor_width=input_shape[2]
        self.model_tensor_height=input_shape[1]
        # this is the control to start/stop
        self.is_ready = False
        self.info = None
        self.thread = None
        self.pipe = multiprocessing.Pipe()


    # This is the threading function
    def handle_inference(self):
        """
        This is a LabelStudio API. Will need to pull down the image from GCS, so need authentication
        """
        log.info("In Thread, IsReady: {}, --- info: {}".format(self.is_ready, self.info))
        while self.is_ready:
            task = self.pipe[OUTPUT_PIPE].recv()
            if task is None:
                continue  # No tasks on the pipe

            task_id = task.id

            # Download image to somewhere local
            image_base64 = self.get_image_file(task.image)

            decoded_image = base64.b64decode(image_base64)
            # Convert the image to a NumPy array
            image = cv2.imdecode(np.frombuffer(decoded_image, np.uint8), cv2.IMREAD_COLOR)
            height, width, channel = image.shape

            # Run predict on the single image
            inference_result: InferenceResponse = self.predict(image_base64, True)

            log.info("Inference Results for Task: {} => {}".format(task_id, inference_result))

            image_results = []

            # Convert to Label Studio format
            for detected in inference_result.detected_items:
                # IF label_studio_info is empty/None, then the "setup" has not run
                # TODO: Get the orig width/height to calculate percentage of the w/h out of 100
                detected_x = (detected.min_x * 100)/width
                detected_y = (detected.min_y * 100)/height
                detected_width = (detected.max_x * 100)/width - detected_x
                detected_height = (detected.max_y * 100)/height - detected_y
                log.warning("Detected: {},{} to {},{}".format(detected_x, detected_y, detected_width, detected_height))
                res = {
                    'from_name': self.info.from_name,
                    'to_name': self.info.to_name,
                    'type': 'rectanglelabels',
                    "source": "$image",
                    'value': {
                        'rectanglelabels': [detected.label],
                        'x': detected_x,
                        'y': detected_y,
                        'rotation': 0,
                        'width': detected_width,
                        'height': detected_height,
                    },
                    'score': detected.confidence
                }
                image_results.append(res)

            log.info("Call LabelStudio with results of: " + str(image_results))

            response_obj = {
                'result': json.dumps(image_results),
                'created_ago': '1 minutes',
                'model_version': self.get_model_version(),
                'task': task_id
            }
            log.debug("Submitting: {}".format(response_obj))

            headers = {'Authorization': f'Token {self.info.access_token}',
                       'Content-type': 'application/json; charset=UTF-8'}

            log.info("Headers: " + str(headers))
            # TODO: These are not "pre-annotation" predictions, these are "annotations"
            url = "http://{}/api/tasks/{}/annotations".format(f"{Config.LABEL_STUDIO_HOST}:{Config.LABEL_STUDIO_PORT}", task_id)
            # url = "http://{}/api/predictions".format("127.0.0.0:8080")
            response = requests.post(url, headers=headers, json=response_obj)

            if response.status_code > 204:
                log.error("Failed sending back to LabelStudio. Response: {}".format(response))

    # This is what is called by frontend
    def batch_inference(self, tasks: List[Task]) -> bool:
        # Send tasks to Pipe
        for task_obj in tasks:
            # Add each task to the pipe
            task = Task(task_obj)
            log.debug("Sending Task [{}]".format(task))
            self.pipe[INPUT_PIPE].send(task)

        if tasks.__len__() > 0:
            # Tasks sent to pipe
            self.thread = threading.Thread(target=self.handle_inference, args=())

            while self.info is None:
                continue

            self.is_ready = True
            self.thread.start()
        else:
            self.is_ready = False
            self.thread = None
        return True

    def setup(self, info: Setup) -> bool:
        self.info = info
        log.warning("Setting Info with {}".format(self.info))
        return True

    def get_model_version(self):
        if self.info is not None:
            return self.info.model_version
        else:
            return self.model_version

    def get_model_path(self) -> str:
        """Returns the model path"""
        return self.model_path

    def get_metadata(self):
        # Assuming a typical image model, the dimensions would be:
        model_height = self.model_tensor_height
        model_width = self.model_tensor_width

        return Metadata(None, model_height, model_width, self.model_params.labels)

    def predict(self, encoded_base64_image, resize) -> InferenceResponse:
        """ Performs an inference on the image returning a single InferenceResponse """

        decoded_image = base64.b64decode(encoded_base64_image)

        # Convert the image to a NumPy array
        nparr = np.frombuffer(decoded_image, np.uint8)

        log.debug("Size of decoded image array: {}".format(nparr.size))
        # Get new instance of TFLite Interpreter (this is important since the object is NOT thread safe
        interpreter = tf.lite.Interpreter(model_path=self.get_model_path())
        input_details = interpreter.get_input_details()
        # Get expected tensor dimensions
        input_shape = input_details[0]['shape']
        model_height = input_shape[1]
        model_width = input_shape[2]
        model_expected_width=model_width
        model_expected_height=model_height

        # FIXME: @cam -- if the resize is false, the `original_width` is not a variable in scope
        if resize:
            nparr, original_width, original_height, = self.downsize(nparr=nparr, new_width=model_expected_width, new_height=model_expected_height)
            log.debug("original height: {}, original width: {}".format(original_height, original_width))
        else: # this is VERY sloppy, need to look at what is going on here to fix
            image = self.get_img_dimensions(nparr)
            original_width, original_height, channel = image.shape
        metadata = self.get_metadata()
        log.debug("npshape {}".format(nparr.shape))
        nparr = nparr.reshape((1, int(metadata.get_width()), int(metadata.get_height()), 3))
        log.debug("Shape of 'decoded_image':", nparr.shape)
        # Decode the array to an OpenCV image format
        detected_items = []

        # Get input and output tensors
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()

        # Run inference
        interpreter.allocate_tensors()
        interpreter.set_tensor(input_details[0]['index'], nparr)
        interpreter.invoke()

        # Get the output
        output_boxes = interpreter.get_tensor(output_details[0]['index'])
        output_classes = interpreter.get_tensor(output_details[1]['index'])
        output_scores = interpreter.get_tensor(output_details[2]['index'])
        num_detections = int(interpreter.get_tensor(output_details[3]['index']))

        # Postprocess output
        for i in range(num_detections):

            score = float(output_scores[0][i])
            if score > self.model_params.score_minimum:
                class_id = int(output_classes[0][i])
                label = self.model_params.labels[class_id]

                box = output_boxes[0][i]

                # Process the bounding box coordinates (they may be in normalized form)
                ymin, xmin, ymax, xmax = box
                # TODO: original_width/height ONLY works if "resize" is enabled. This is a bug
                xmin = int(xmin * nparr.shape[1] * original_width)
                xmax = int(xmax * nparr.shape[1] * original_width)
                ymin = int(ymin * nparr.shape[2] * original_height)
                ymax = int(ymax * nparr.shape[2] * original_height)

                detected_item = Detected_Item(xmin, ymin, xmax, ymax, label, score)
                detected_items.append(detected_item)

        log.debug("detected labels: {}".format(detected_items))
        response = InferenceResponse(detected_items)

        top_end = 100 / Config.FEEDBACK_PCT
        random_number = random.randint(0, top_end)

        # % chance of triggering
        if 1 == random_number:
            # Capture the feedback every X requests
            self.save_feedback(decoded_image, response.detected_items)

        return response

    @staticmethod
    def save_feedback(image_data, detected_items):

        # Don't save empty feedback
        if detected_items.__len__() <= 0:
            return

        log.debug("Recording Feedback")
        # String of current date-time
        date_time_string = datetime.now().strftime("%d-%m-%Y-%H-%M-%S")

        # Write the bytes to a .jpg file
        with open(f"{Config.FEEDBACK_PATH}/{date_time_string}-image.jpg", "wb") as image_file:
            image_file.write(image_data)
        # Write the bytes to a .jpg file
        with open(f"{Config.FEEDBACK_PATH}/{date_time_string}-detected.yaml", "w") as yaml_file:
            yaml.dump(detected_items,
                      yaml_file,
                      default_flow_style=False)

    def downsize(self, nparr, new_height, new_width):
        width_ratio, height_ratio = 1, 1
        image = self.get_img_dimensions(nparr)
        height, width, channel = image.shape

        if new_width is None and new_height is None:
            log.debug("stopped")
            return nparr, width, height  # No resizing needed, return original image dimensions

        if new_width is not None:
            width_ratio = width / new_width
            log.debug("width ratio {}".format(width_ratio))

        if new_height is not None:
            height_ratio = height / new_height
            log.debug("height ratio {}".format(height_ratio))

        resized_image = cv2.resize(image, (new_width, new_height))
        rgb_image = cv2.cvtColor(resized_image, cv2.COLOR_BGR2RGB)

        return rgb_image, width_ratio, height_ratio

    @staticmethod
    def get_img_dimensions(nparr):
        return cv2.imdecode(nparr, cv2.IMREAD_COLOR)

    @staticmethod
    def get_image_file(image_uri) -> Optional[str]:
        """
        LabelStudio gives a URI to an image, this pulls that down from GCS
        """
        # gs://price-a-tray-mediapipe-images/train-raw-images/combofeb23_0008.png
        base_folder = Config().IMAGE_DOWNLOAD_FOLDER
        google_project = Config().GOOGLE_PROJECT

        # Initialise a client
        storage_client = storage.Client(google_project)
        # Create a bucket object for our bucket

        match = re.match(r'gs://(?P<bucket>.+?)/(?P<object>.+)', image_uri)
        if match:
            # get the bucket name
            bucket_name = match.group('bucket')
            # get the remote filepath string
            object_name = match.group('object')
            # TODO: Do more to create a local image file name, this only swaps / with _
            local_file = base_folder + "/" + object_name.replace("/", "_")

            # TODO: Check if the file already exists
            # if True:
            # Get the bucket
            bucket = storage_client.get_bucket(bucket_name)
            # Create a blob object from the filepath
            blob = bucket.blob(object_name)
            # Download the file to a destination (base folder + object-path to be the full image
            # TODO: handle download failures
            blob.download_to_filename(local_file)

            if os.path.isfile(local_file):
                log.info("Downloaded file {}".format(local_file))
                with open(local_file, "rb") as img_file:  # Open image in binary mode
                    encoded_string = base64.b64encode(img_file.read())
                    return encoded_string.decode('utf-8')
            else:
                log.error("Failed to download: {}".format(local_file))
        else:
            raise ValueError('Invalid GS URL: {}'.format(image_uri))
