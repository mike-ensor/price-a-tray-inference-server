import logging

from flask import current_app

from app.main.controller_models import InferenceResponse, ModelParameters
from app.main.inference_models import EdgeInferenceModel, CloudInferenceModel
from app.main.utilities import Utilities
from config import Config

log = logging.getLogger(current_app.name)


class MLController(object):

    """
    Class contains the model which can be of type "cloud" or "edge", or neither and no functionality is done

    Primary responsibility is providing a unified API to inference models
    """
    _instance = None
    model_initialized = None
    model = None
    is_label_studio_setup = None

    def __init__(self):
        pass

    def __new__(cls):
        if cls._instance is None:
            print('Creating new instance of MLController')
            cls._instance = super(MLController, cls).__new__(cls)
            # Put any initialization here.
        return cls._instance

    def initialize(self, model_params: ModelParameters = None, model_type="edge"):
        """ Initialize the ML Controller for type of model instance and configuration """
        self.is_label_studio_setup = False
        if model_type == "edge":
            log.info("Edge Inference Model selected")
            os_model_path = Utilities.get_full_os_path(Config.EDGE_MODEL_PATH)

            self.model = EdgeInferenceModel(model_params, os_model_path)
            self.model_initialized = True
        elif model_type == "cloud":
            log.info("Cloud Inference Model selected")
            self.model = CloudInferenceModel(model_params)
            self.model_initialized = True
        else:
            log.warning("Neither Model Added: {}".format(model_type))
            self.model = None
            self.model_initialized = False

    def is_initialized(self):
        """ Has the Singleton class been initialized? """
        return self.model_initialized

    def has_been_setup(self):
        """ Returns true if the LabelStudio setup has been called and successfully setup """
        return self.is_label_studio_setup

    def setup(self, info) -> bool:
        """ Label Studio setup information is stored on the Singleton """
        self.is_label_studio_setup = self.model.setup(info)
        return self.is_label_studio_setup

    def inference(self, base64_encoded_image, resize) -> InferenceResponse:
        return self.model.predict(base64_encoded_image, resize)

    def batch(self, tasks) -> bool:
        return self.model.batch_inference(tasks)

    def metadata(self):
        return self.model.get_metadata()
