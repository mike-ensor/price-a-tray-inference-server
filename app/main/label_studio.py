import logging
from typing import List

import requests
from PIL.Image import Image
from flask import current_app
from label_studio_tools.core.label_config import parse_config
from requests.auth import HTTPBasicAuth

from app.main.labelstudio_datamodel import Setup, Task
from app.main.utilities import Utilities
from config import Config

log = logging.getLogger(current_app.name)


class LabelStudio:

    def __init__(self, setup: Setup):

        self.ls_server_host = Config.LABEL_STUDIO_HOST
        self.ls_server_port = Config.LABEL_STUDIO_PORT

        self.project = setup.project
        self.parsed_label_config = parse_config(setup.label_schema)
        self.hostname = setup.hostname

        self.user = "label-studio-user"
        self.access_token = setup.access_token

        from_name, schema = list(self.parsed_label_config.items())[0]
        self.from_name = from_name
        self.to_name = schema['to_name'][0]
        self.labels = schema['labels']

    def send_predictions(self, result):
        """ Sends the inference to Label Studio """
        url = f'http://{self.ls_server_host}:{self.ls_server_port}/annotations/git/api/predictions/'
        auth = HTTPBasicAuth(self.user, self.access_token)
        res = requests.post(url, auth=auth, json=result)
        if res.status_code != 200:
            log.warning("WARNING: {}".format(res))

    def predict(self, tasks: List[Task]) -> bool:
        """Add predict logic here and call `send_predictions` for each task"""

        # TODO: for each task
        # 1. Pull down the image from GCS
        # 2. Run the inference against the item
        # 3. Run the
        for task in tasks:
            uri = task.data['image']

            # Convert repo:// to gs:// to local file so encoding can be done
            url = self.uri_to_url(uri)

            # Download image to file
            image_path = self.download_url(url)  # Should be this, then it can be cached

            # Create an Image (Pillow) object for sizing
            img = Image.open(image_path)
            img_w, img_h = img.size

            # Convert to base64 encoded image for inference input
            data = image_path.read()
            encoded_content = Utilities.base64_encode_image(data)

            # Run inference
            ## TODO: Run the inference
            data = "{ 'image': '{}'}".format(encoded_content)
            instance = requests.post(url="http://localhost:5000/inference", data=data)

            # Add to inference dictionary (to loop through later?)
            instances = [instance]

            lowest_conf = 2.0

            img_results = []

            for obj in instances:
                x, y, w, h, conf, cls = obj
                cls = int(cls)
                conf = float(conf)
                # Convert to 0..100, not 0.0 to 1.0 (LabelStudio needs 0-100, not floating)
                x = 100 * float(x - w / 2) / img_w
                y = 100 * float(y - h / 2) / img_h
                w = 100 * float(w) / img_w
                h = 100 * float(h) / img_h
                if conf < lowest_conf:
                    lowest_conf = conf
                label = self.labels[cls]

                # LabelStudio output
                img_results.append({
                    'from_name': self.from_name,
                    'to_name': self.to_name,
                    'type': 'rectanglelabels',
                    'value': {
                        'rectanglelabels': [label],
                        'x': x,
                        'y': y,
                        'width': w,
                        'height': h,
                    },
                    'score': conf
                })

            result = {
                'result': img_results,
                'model_version': self.model_version,
                'task': task.id
            }

            if lowest_conf <= 1.0:
                result['score'] = lowest_conf

            self.send_predictions(result)
            return True

    def uri_to_url(self, uri):
        # "gs://<bucket>/<model-images>/<folders>/output_0021.png"  URI (from results.json)
        if uri.startswith('http'):
            return uri
        elif uri.startswith('repo://'):
            link_data = uri.split("repo://")[-1].split("/")
            commit, tree_path = link_data[0], "/".join(link_data[1:])
            return f"http://<path-to-gcs-bucket"

        raise FileNotFoundError(f'Unknown URI {uri}')

    def download_url(self, url) -> str:
        # Pull image from GCS bucket to local, drop in a folder and return the OS path to that file

        pass
