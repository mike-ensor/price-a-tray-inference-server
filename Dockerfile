FROM python:3.9-slim
# Get all of the packages needed for the runtime

RUN cd /tmp && \
          apt-get update && \
          apt-get install -y \
		          libudev-dev \
                  libgl1 \
                  libglib2.0-0 \
                  libsm6 \
                  libxext6
WORKDIR /app

# Create virtual environment
#RUN python3 -m venv venv
#ENV PATH=/app/venv/bin/:$PATH

COPY . .
RUN python3 -m pip install --no-cache-dir --upgrade pip && pip install --no-cache-dir -r requirements.txt

EXPOSE 5000

ENV PYTHONUNBUFFERED=1
ENV PYTHONPATH=/app/venv/lib/python3.9/site-packages
ENV PATH=/app/venv/bin/:$PATH
ENV FLASK_APP=app
ENV FLASK_ENV=production

CMD ["python" , "/app/run.py"]
